import type {Plugin} from "rollup";
import type {FilterPattern} from "@rollup/pluginutils";
import {createFilter} from "@rollup/pluginutils";
import {extname} from "path";
import {
    createSource,
    type TwingEnvironment,
    type TwingParsingError
} from "twing";
import {visit} from "./visitor";
import type {TransformResult} from "rollup";

const createTwigPlugin: (environment: TwingEnvironment, options?: {
    include?: FilterPattern,
    exclude?: FilterPattern,
}) => Plugin = (environment, options = {}) => {
    const filter = createFilter(options.include, options.exclude);
    const extensions = ['.twig'];
    
    const plugin: Plugin = {
        name: 'twig',
        transform(
            this,
            code,
            id
        ) {
            if (!filter(id) || !extensions.includes(extname(id))) {
                return;
            }

            try {
                const ast = environment.parse(environment.tokenize(createSource(id, code)));

                return visit(ast, environment, id)
                    .then<TransformResult>((encounteredTemplates) => {
                        let index = 0;

                        const dependencies = encounteredTemplates.map(([templateName, resolvedName]) => {
                            return `import template${index} from "${resolvedName}";
templates.set('${templateName}', template${index++});`;
                        }).join('');

                        const generatedCode = `import {createTemplate} from "twing";

const templates = new Map();

${dependencies}

const template = createTemplate(${JSON.stringify(ast)});
const baseLoadTemplate = template.loadTemplate;

template.loadTemplate = (executionContext, identifier) => {
    return templates.has(identifier) ? Promise.resolve(templates.get(identifier)) : baseLoadTemplate(executionContext, identifier);
};

for (const [, embeddedTemplate] of template.embeddedTemplates) {
    embeddedTemplate.loadTemplate = template.loadTemplate;
}

export default template;`

                        return {
                            code: generatedCode,
                            map: {
                                mappings: ''
                            }
                        };
                    });
            }
            catch (error) {
                const {message, location} = (error as TwingParsingError);
                const {line, column} = location!;

                return this.error(new Error(message), {
                    line,
                    column: column - 1
                });
            }
        }
    };

    return plugin;
};

export default createTwigPlugin;