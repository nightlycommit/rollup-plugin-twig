# @nightlycommit/rollup-plugin-twig
[![NPM version][npm-image]][npm-url] [![Build Status][build-image]][build-url] [![Coverage percentage][coveralls-image]][coveralls-url]

A Rollup plugin for seamless integration between Rollup and Twig, fueled by [Twing](https://www.npmjs.com/package/twing).

## Requirements

This plugin requires at least [Twing](https://www.npmjs.com/package/twing) 6.0.0.

## Installation

The recommended way to install the package is via npm:

```shell
npm install @nightlycommit/rollup-plugin-twig --save-dev
```

Note that [Twing](https://www.npmjs.com/package/twing) is a peer dependency of this plugin that needs to be installed separately.

## Usage

Create a Rollup configuration file, import the plugin factory and add an instance to the list of `plugins`:

```js
// rollup.config.mjs
import {createEnvironment, createFilesystemLoader} from 'twing';
import createTwigPlugin from '@nightlycommit/rollup-plugin-twig';
import * as fs from "fs";

const environment = createEnvironment(createFilesystemLoader(fs));

export default {
    input: 'src/index.js',
    output: {
        dir: 'output'
    },
    plugins: [
        createTwigPlugin(environment)
    ]
};
```

Then, in your JavaScript sources, import Twig files and execute the `render` method of the exported module:

```javascript
import {render} from "../templates/index.twig";

render({}).then(console.log);
```

Note that to import Twig files in TypeScript sources, you need to make the compiler aware of the existence of the `*.twig` type by either referencing the type in the code...

```ts
/// <reference types="@nightlycommit/rollup-plugin-twig" />
import {render} from "../templates/index.twig";

render({}).then(console.log);
```

...or by adding `<path to node_modules>/@nightlycommit/rollup-plugin-twig` to the `typeRoots` entry of your TypeScript configuration, as explained [there](https://www.typescriptlang.org/tsconfig#typeRoots).

## Plugin factory signature

```typescript
type PluginFactory = (environment: TwingEnvironment, options?: {
    exclude?: string | Array<string>;
    include?: string | Array<string>;
}) => Plugin;
```

### environment

An instance of `TwingEnvironment` capable of loading and parsing the templates.

### options

#### exclude

A picomatch pattern, or array of patterns, which specifies the files in the build the plugin should ignore. By default, no files are ignored.

#### include

A picomatch pattern, or array of patterns, which specifies the files in the build the plugin should operate on. By default, all `.twig` files are targeted.

## Twig module signature

Twig files imported using this plugin are exposed to your code in the form of templates with the following signature:

```typescript
import type {TwingTemplate} from "twing";

interface Template {
    render: TwingTemplate["render"];
}
```
Please refer to [Twing](https://www.npmjs.com/package/twing)'s documentation for more information.

[npm-image]: https://badge.fury.io/js/@nightlycommit%2Frollup-plugin-twig.svg
[npm-url]: https://npmjs.org/package/@nightlycommit/rollup-plugin-twig
[build-image]: https://gitlab.com/nightlycommit/rollup-plugin-twig/badges/main/pipeline.svg
[build-url]: https://gitlab.com/nightlycommit/rollup-plugin-twig/-/pipelines
[coveralls-image]: https://coveralls.io/repos/gitlab/nightlycommit/rollup-plugin-twig/badge.svg
[coveralls-url]: https://coveralls.io/gitlab/nightlycommit/rollup-plugin-twig
