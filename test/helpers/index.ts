import {
    TwingFilesystemLoaderFilesystem,
    TwingLoader,
    TwingTemplate,
    createFilesystemLoader
} from "twing";
import {Plugin} from "rollup";
import {relative} from "path";

export const createFilesystem = (): Record<string, string> => {
    return {
        'basic/index.twig': `Hello {{ world }}!`,
        'missing/index.twig': `{{ include("./missing.twig") }}`,
        
        'include-function/index.twig': `{{ include("../partials/hello.twig") }}`,
        'include-tag/index.twig': `{% include foo ? '../partials/hello.twig' : './bar.twig' %}
{% include foo ? '../partials/hello.twig' : not foo ? './foo-2.twig' : './bar.twig' %}
{% include ['./missing.twig', '../partials/hello.twig'] %}
===
{% include bar ? '../partials/hello.twig' : './bar.twig' %}
{% include bar ? '../partials/hello.twig' : not bar ? './foo-2.twig' : './bar.twig' %}
{% include ['./missing.twig', '../partials/hello.twig'] %}`,
        'include-tag/bar.twig': `BAR`,
        'include-tag/foo-2.twig': `FOO 2`,

        'dynamic-include-tag/index.twig': `{% include foo %}`,
        
        'extends-tag/index.twig': `{% extends "./parent.twig" %}`,
        'extends-tag/parent.twig': `{% block content %}
    Parent content
{% endblock %}
`,
        
        'from-tag/index.twig': `{% from "./macros.twig" import hello %}
{{ hello("world") }}
`,
        'from-tag/macros.twig': `{% macro hello(world) %}
    Hello {{ world }}!
{% endmacro %}
`,

        'import-tag/index.twig': `{% import "../from-tag/macros.twig" as macros %}
{{ macros.hello("world") }}
`,
        'import-self-tag/index.twig': `{% import _self as me %}
{% macro hello(world) %}
Hello {{ world }}!
{% endmacro %}
{{ me.hello(world) }}
`,
        
        'embed-tag/index.twig': `{% embed "./skeleton.twig" %}
    {% block footer %}
        Custom footer
    {% endblock %}
{% endembed %}`,
        'embed-tag/skeleton.twig': `<div class="content">
{% block content %}
    Skeleton content
{% endblock %}
</div>
<div class="footer">
{% block footer %}
    Skeleton footer
{% endblock %}
</div>`,

        'embed-tag-and-extends-tag/index.twig': `{% embed "./skeleton.twig" %}
    {% block footer %}
        Custom footer
    {% endblock %}
{% endembed %}`,
        'embed-tag-and-extends-tag/skeleton.twig': `{% extends "../extends-tag/parent.twig" %}
{% block content %}
    <div class="header">
    {% block header %}
        Skeleton header
    {% endblock %}
    </div>
    <div class="content">
        {{ parent() }}
    </div>
    <div class="footer">
    {% block footer %}
        Skeleton footer
    {% endblock %}
    </div>
{% endblock %}`,

        'partials/hello.twig': `Hello {{ world }}!`,

        'parent-function/index.twig': `{% extends "./parent.twig" %}
{% block content %}
    {{ parent() }} and custom content
{% endblock %}`,
        'parent-function/parent.twig': `{% block content %}
    Parent content
{% endblock %}
`,
        'block-function/index.twig': `{% extends "./parent.twig" %}
{% block title %}Custom content{% endblock %}`, // todo: {% extends "parent.twig" %} does not work
        'block-function/parent.twig': `<title>{% block title %}{% endblock %}</title>
<h1>{{ block("title") }}</h1>
<div>{{ block("content", "./blocks.twig") }}</div>`,
        'block-function/blocks.twig': `{% block content %}Content{% endblock %}`,
        
        'parsing-error/index.twig': `{{ foo }`,
        
        'unsupported-module/index.js': `5;`
    };
};

export const createLoader = (): TwingLoader => {
    return createFilesystemLoader(createFilesystemLoaderFilesystem());
};

export const createFilesystemLoaderFilesystem = (): TwingFilesystemLoaderFilesystem => {
    const filesystem = createFilesystem();

    return {
        readFile: (path, callback) => {
            callback(null, Buffer.from(filesystem[path]));
        },
        stat: (path, callback) => {
            if (filesystem[path]) {
                callback(null, {
                    isFile() {
                        return true;
                    },
                    mtime: new Date()
                })
            } else {
                callback(null, null);
            }
        }
    };
};

export const createFilesystemPlugin = (): Plugin => {
    const fileSystem = createFilesystem();

    return {
        name: 'filesystem',
        resolveId: (id) => {
            const relativeId = relative('.', id);
            
            if (fileSystem[relativeId]) {
                return relativeId;
            }

            return null;
        },
        load: (id) => {
            const content = fileSystem[id];

            return content || null;
        }
    };
};

export const evaluate = (code: string): {
    render: TwingTemplate["render"]
} => {
    const evaluator = new Function('require', `module = {};
${code}

return module.exports;
`);

    return evaluator(require);
};