import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('embed tag and extends tag together', ({test}) => {
    test('returns a legit template-like instance', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'embed-tag-and-extends-tag/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({}));

            return template.render(environment, {world: 'world'})
                .then((output) => {
                    same(moduleIds, [
                        'extends-tag/parent.twig',
                        'embed-tag-and-extends-tag/skeleton.twig',
                        'embed-tag-and-extends-tag/index.twig']
                    );
                    same(output, `    <div class="header">
            Skeleton header
        </div>
    <div class="content">
            Parent content

    </div>
    <div class="footer">
            Custom footer
        </div>
`);
                });
        }).finally(end);
    });
});