import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createEnvironment} from "twing";
import {rollup, RollupError} from "rollup";
import {createFilesystemPlugin, createLoader} from "../helpers/index";

tape('parsing error', ({test}) => {
    test('throws an error with position', ({fail, same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'parsing-error/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then(() => {
            fail();
        }).catch((error) => {
            const {message, loc, hook, plugin, id} = error as RollupError;
            
            same(message, 'Unexpected "}" in "parsing-error/index.twig" at line 1, column 8.');
            same(loc, {
                column: 7,
                file: 'parsing-error/index.twig',
                line: 1
            });
            same(hook, 'transform');
            same(plugin, 'twig');
            same(id, 'parsing-error/index.twig');
        }).finally(end);
    });
});