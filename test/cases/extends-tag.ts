import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('extends tag', ({test}) => {
    test('returns a legit template-like instance', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'extends-tag/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({}));

            return template.render(environment, {world: 'world'})
                .then((output) => {
                    same(moduleIds, [
                        'extends-tag/parent.twig',
                        'extends-tag/index.twig'
                    ]);
                    same(output, `    Parent content
`);
                });
        }).finally(end);
    });
});