import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('include tag', ({test}) => {
    test('returns a legit template-like instance', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'include-tag/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({}));

            return template.render(environment, {foo: true})
                .then((output) => {
                    same(moduleIds, [
                        'partials/hello.twig',
                        'include-tag/bar.twig',
                        'include-tag/foo-2.twig',
                        'include-tag/index.twig'
                    ]);
                    same(output, `Hello !Hello !Hello !===
BARFOO 2Hello !`);
                });
        }).finally(end);
    });
});