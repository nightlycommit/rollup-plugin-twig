import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('block function', ({test}) => {
    test('returns a legit template-like instance', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'block-function/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({}));

            return template.render(environment, {world: 'world'})
                .then((output) => {
                    same(moduleIds, [
                        'block-function/blocks.twig',
                        'block-function/parent.twig',
                        'block-function/index.twig'
                    ]);
                    same(output, `<title>Custom content</title>
<h1>Custom content</h1>
<div>Content</div>`);
                });
        }).finally(end);
    });
});