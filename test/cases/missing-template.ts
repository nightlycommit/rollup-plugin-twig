import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('missing template', ({test}) => {
    test('throws an error with position', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'missing/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({}));

            return template.render(environment, {world: 'world'})
                .then((output) => {
                    same(moduleIds, [
                        'partials/hello.twig',
                        'include-tag/index.twig'
                    ]);
                    same(output, 'Hello world!');
                });
        }).catch((error) => {
            const {message} = error as Error;
            
            same(message, 'Unable to find template "./missing.twig" in "missing/index.twig" at line 1, column 4.');
        }).finally(end);
    });
});