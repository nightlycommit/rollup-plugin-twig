import tape from "tape";
import {createEnvironment} from "twing";
import {createFilesystemPlugin, createLoader} from "../helpers/index";
import createTwigPlugin from "../../src/lib/plugin";
import {rollup} from "rollup";

tape('unsupported module', ({test}) => {
    test('ignores unsupported modules', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'unsupported-module/index.js',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code} = output.output[0];

            same(code, `'use strict';

`);
        }).finally(end);
    });
});