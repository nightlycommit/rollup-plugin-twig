import tape from "tape";
import createTwigPlugin from "../../src/lib/plugin";
import {createArrayLoader, createEnvironment} from "twing";
import {rollup} from "rollup";
import {createFilesystemPlugin, createLoader, evaluate} from "../helpers/index";

tape('dynamic include tag', ({test}) => {
    test('throws an error', ({same, end}) => {
        const environment = createEnvironment(createLoader());
        const plugin = createTwigPlugin(environment);

        return rollup({
            input: 'dynamic-include-tag/index.twig',
            plugins: [
                createFilesystemPlugin(),
                plugin
            ]
        }).then((build) => {
            return build.generate({
                format: "cjs"
            })
        }).then((output) => {
            const {code, moduleIds} = output.output[0];
            const template = evaluate(code);
            const environment = createEnvironment(createArrayLoader({
                bar: 'Hello world!'
            }));

            return template.render(environment, {foo: 'bar'})
                .then((output) => {
                    same(moduleIds, [
                        'dynamic-include-tag/index.twig'
                    ]);
                    same(output, 'Hello world!');
                });
        }).finally(end);
    });
});